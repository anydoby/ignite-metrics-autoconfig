package com.anydoby.ignite;

import static java.util.Arrays.asList;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestCacheConfigurer {

    @Bean
    public Ignite ignite() {
        final IgniteConfiguration configuration = new IgniteConfiguration();
        final TcpDiscoveryVmIpFinder ipFinder = new TcpDiscoveryVmIpFinder();
        ipFinder.setAddresses(asList("localhost"));
        final TcpDiscoverySpi discoverySpi = new TcpDiscoverySpi();
        discoverySpi.setIpFinder(ipFinder);
        configuration.setDiscoverySpi(discoverySpi);

        final CacheConfiguration<String, String> cache = new CacheConfiguration<>();
        cache.setName("test");

        configuration.setCacheConfiguration(cache);
        return Ignition.start(configuration);
    }

}
