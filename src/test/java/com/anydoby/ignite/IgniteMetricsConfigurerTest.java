package com.anydoby.ignite;

import static java.lang.Double.parseDouble;
import static java.util.Arrays.stream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.metrics.export.prometheus.PrometheusMetricsExportAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.prometheus.PrometheusMeterRegistry;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { IgniteMetricsConfigurerTest.TestConfig.class })
@TestPropertySource(properties = { "ignite.metrics.frequency=10" })
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
public class IgniteMetricsConfigurerTest {

    @Configuration
    @Import({ MetricsAutoConfiguration.class, PrometheusMetricsExportAutoConfiguration.class })
    @ComponentScan("com.anydoby.ignite")
    public static class TestConfig {
    }

    @Autowired
    Ignite ignite;

    @Autowired
    MeterRegistry metrics;

    @Test
    public void checkStats() throws InterruptedException {
        final IgniteCache<String, String> recos = ignite.cache("test");
        recos.put("dummy", "value");

        for (int i = 0; i < 1000; i++) {
            recos.get("dummy");
        }
        recos.get("wrong-key");
        Thread.sleep(1000);

        final PrometheusMeterRegistry r = (PrometheusMeterRegistry) metrics;
        final String[] scrape = stream(r.scrape().split("\n")).map(String::trim).filter(s -> s.startsWith("cache_")).toArray(String[]::new);
        assertEquals(0, metric("cache_evictions_total{cache=\"test\",}", scrape), 0);
        assertEquals(1, metric("cache_puts_total{cache=\"test\",}", scrape), 0);
        assertEquals(1, metric("cache_size{cache=\"test\",}", scrape), 0);
        assertEquals(1, metric("cache_gets_total{cache=\"test\",result=\"miss\",}", scrape), 0);
        assertEquals(1000, metric("cache_gets_total{cache=\"test\",result=\"hit\",}", scrape), 0);
        assertEquals(0, metric("cache_gets_averagetime{cache=\"test\",}", scrape), 0.0);
        assertTrue(metric("cache_puts_averagetime{cache=\"test\",}", scrape) > 0);

        stream(scrape).forEach(System.out::println);
    }

    private static double metric(String metric, String[] scrape) {
        for (final String string : scrape) {
            if (string.startsWith(metric)) {
                return parseDouble(string.replace(metric, "").trim());
            }
        }
        fail("Metric not found: " + metric);
        return -1;
    }

}
