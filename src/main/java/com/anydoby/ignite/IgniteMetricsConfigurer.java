package com.anydoby.ignite;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.ignite.Ignite;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

import io.micrometer.core.instrument.MeterRegistry;

/**
 * This autoconfiguration enables metrics reporting for ignite caches.
 */
@Configuration
@ConditionalOnClass({ MeterRegistry.class })
@ConditionalOnProperty(prefix = "ignite.metrics", name = "enabled", havingValue = "true", matchIfMissing = true)
public class IgniteMetricsConfigurer {

    @Autowired
    Ignite ignite;

    @Autowired
    MeterRegistry metricsRegistry;

    @Value("${ignite.metrics.frequency:5000}")
    Integer metricsFrequencyMillis = 5000;

    private final List<IgniteCacheMetrics> metrics = new ArrayList<>();

    @PostConstruct
    public void registerCacheMetrics() throws BeansException {
        ignite.cacheNames().forEach(cache -> {
            ignite.cache(cache).enableStatistics(true);
            metrics.add(IgniteCacheMetrics.monitor(metricsRegistry, ignite, cache, metricsFrequencyMillis));
        });
    }

    @PreDestroy
    public void unregisterCacheMetrics() {
        metrics.forEach(m -> m.unregister(metricsRegistry));
    }

}
