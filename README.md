# Apache Ignite metrics configurator

This project contains a Spring Boot metrics autoconfigurer for Apache Ignite. It uses Micrometer framework to abstract metrics reporting.

Frequency of metrics refresh can be controlled using the `ignite.metrics.frequency` property (milliseconds). If you need to disable metrics monitoring temporarily, turn it off with `ignite.metrics.enabled`.

Internally this library runs a metrics refresh task using Ignite's scheduler.

Below is the list of all exposed metrics:

|metric    | description   |
|:---|:---|
| cache_gets_total{cache="<cache-name>",result="hit",}   |  total number of cache hits  |
| cache_gets_total{cache="<cache-name>",result="miss",}   |  total number of cache misses  |
| cache_puts_total{cache="<cache-name>",}  |  total number of cache puts |
| cache_evictions_total{cache="<cache-name>",} | total number of cache evictions |
| cache_size{cache="<cache-name>",} | current cache size (all nodes, all regions) |
| cache_gets_averagetime{cache="<cache-name>",} | average time in millis per cache get |
| cache_puts_averagetime{cache="<cache-name>",} | average time in millis per cache put |

# Usage

`@Import` the following autoconfiguration class to your Spring configuration: `com.anydoby.ignite.IgniteMetricsConfigurer`.